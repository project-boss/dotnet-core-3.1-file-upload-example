import { Component, OnInit, Inject } from '@angular/core';
import { User, UploadService } from '../services/upload.service';
import { HttpClient,HttpEvent, HttpEventType  } from '@angular/common/http';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  name = 'Angular 4';
  urls = [];
  data: string[] = [];
  constructor(private uploadService: UploadService, private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }

  ngOnInit() {
  }

  onSelectFile(event) {
    this.data = event.target.files
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();

        reader.onload = (event: any) => {
          // console.log(event.target.result);
          this.urls.push(event.target.result);
        }
        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }
  upload() {
    const formData = new FormData();
    for (var i = 0; i < this.data.length; i++) {
      formData.append("files", this.data[i]);
    }
    formData.append('Id', '1')
    formData.append('Name', 'Palm')
    this.uploadService.uploadToServer(formData)
      .subscribe(result => {
        console.log(result);
      })
  }

}
