import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
export interface User {
  Id: Number;
  Name: string;
  files: FileList
}
@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }
  public upload(formData) {

    return this.http.post<User>(this.baseUrl + 'api/uploads/sub', formData, {
      reportProgress: true,
      observe: 'events'
    })
  }

  uploadToServer(formData) {
    return this.http.post<any>(this.baseUrl + 'api/uploads/sub', formData, {
      reportProgress: true,
      observe: 'events'
    })
  }

  addUser(name: string, profileImage: FileList): Observable<any> {
    var formData: any = new FormData();
    for (var i = 0; i < profileImage.length; i++) {
      formData.append("files", profileImage[i]);
    }
    formData.append("name", name);
    // formData.append("files", profileImage);

    return this.http.post(this.baseUrl + 'api/uploads/sub', formData, {
      reportProgress: true,
      observe: 'events'
    }).pipe(
      catchError(this.errorMgmt)
    )
  }

  errorMgmt(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
