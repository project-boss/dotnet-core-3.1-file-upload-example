import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { UploadService } from '../services/upload.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
  @ViewChild("fileUpload", { static: false }) fileUpload: ElementRef;
  files = [];

  fileToUpload: FileList = null;

  constructor(private uploadService: UploadService) { }

  uploadImage() {
    const formData = new FormData();
    // for(let i=0; i<this.fileToUpload.length; ++i){
    //   formData.append('files['+i+']',this.fileToUpload.item(i),this.fileToUpload.item(i).name)
    // }
    for (let i = 0; i < this.fileToUpload.length; ++i) {
      var file = this.fileToUpload[i];
      console.log(file);
      formData.append('files[' + ']', file, file.name)
    }
    // formData.append('files', this.fileToUpload[0], this.fileToUpload[0].name);
    // body.append('Id','1')
    formData.append('Name', 'Palm')
  }
  uploadFile(file) {
    const formData = new FormData();
    formData.append('files', file.data);
    file.inProgress = true;
    this.uploadService.upload(formData)
      .pipe(
        map(event => {
          console.log(event);
          switch (event.type) {
            case HttpEventType.UploadProgress:
              file.progress = Math.round(event.loaded * 100 / event.total);
              break;
            case HttpEventType.Response:
              return event;
          }
        }),
        catchError((error: HttpErrorResponse) => {
          file.inProgress = false;
          return of(`${file.data.name} upload failed.`);
        })).subscribe((event: any) => {

          if (typeof (event) === 'object') {

          }
        });
  }

  uploadFiles() {
    this.fileUpload.nativeElement.value = '';
    this.files.forEach(file => {
      this.uploadFile(file);
    });
  }
  onClick() {
    const fileUpload = this.fileUpload.nativeElement;
    fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        console.log('in');
        const file = fileUpload.files[index];
        this.files.push({ data: file, inProgress: false, progress: 0 });
      }
      // this.uploadFiles();
    };
    fileUpload.click();
  }
}
