import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms'
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-multiple-input-example',
  templateUrl: './multiple-input-example.component.html',
  styleUrls: ['./multiple-input-example.component.css']
})
export class MultipleInputExampleComponent implements OnInit {
  //https://jasonwatmore.com/post/2019/06/25/angular-8-dynamic-reactive-forms-example
  dynamicForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    this.dynamicForm = this.formBuilder.group({
      numberOfTickets: ['', Validators.required],
      tickets: new FormArray([])
    });
  }

  // convenience getters for easy access to form fields
  get f() { return this.dynamicForm.controls; }
  get t() { return this.f.tickets as FormArray; }

  onChangeTickets(e) {
    const numberOfTickets = e.target.value || 0;
    if (this.t.length < numberOfTickets) {
      for (let i = this.t.length; i < numberOfTickets; i++) {
        this.t.push(this.formBuilder.group({
          name: ['', Validators.required],
          email: ['', [Validators.required, Validators.email]]
        }));
      }
    } else {
      for (let i = this.t.length; i >= numberOfTickets; i--) {
        this.t.removeAt(i);
      }
    }
  }

  onSubmit() {
    this.submitted = true;
    let formData = new FormData
    formData.append('subjects', JSON.stringify(this.dynamicForm.value.tickets))
    this.http.post('http://localhost:5001/api/member', formData)
      .subscribe(result => {
        console.log('server', result);
      })
    // stop here if form is invalid
    if (this.dynamicForm.invalid) {
      return;
    }
    // console.log(this.dynamicForm.value.tickets);


    // display form values on success
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.dynamicForm.value, null, 4));
  }

  onReset() {
    // reset whole form back to initial state
    this.submitted = false;
    this.dynamicForm.reset();
    this.t.clear();
  }

  onClear() {
    // clear errors and reset ticket fields
    this.submitted = false;
    this.t.reset();
  }
}
