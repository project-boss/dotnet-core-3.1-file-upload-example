import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS, } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { UploadService } from './services/upload.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatButtonModule,
  MatProgressBarModule
} from '@angular/material';
import { UploadComponent } from './upload/upload.component';
import { StoreModule, Store } from '@ngrx/store';
import { ShoppingReducer } from './store/reducers/shopping.reducer';
import { StoreExampleComponent } from './store-example/store-example.component';
import {StoreDevtoolsModule} from '@ngrx/store-devtools'
import { environment } from 'src/environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { ShoppingEffects } from './store/effects/shopping.effect';
import { UploadFileProgressComponent } from './upload-file-progress/upload-file-progress.component';
import { MultipleInputExampleComponent } from './multiple-input-example/multiple-input-example.component';
@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    StoreExampleComponent,
    UploadComponent,
    UploadFileProgressComponent,
    MultipleInputExampleComponent
    
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: FetchDataComponent },
      { path: 'upload', component: UploadComponent },
      { path: 'store', component: StoreExampleComponent },
      { path: 'upload-progress', component: UploadFileProgressComponent },
      { path: 'multiple-add', component: MultipleInputExampleComponent },
    ]),
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    // ReactiveFormsModule,
    MatButtonModule,
    HttpClientModule,
    MatProgressBarModule,
    EffectsModule.forRoot([ShoppingEffects]),
    StoreModule.forRoot({
      shopping:ShoppingReducer,
    }),
    StoreDevtoolsModule.instrument({maxAge:25,logOnly:environment.production})
    // StoreModule.forFeature({})

  ],
  providers: [UploadService],
  bootstrap: [AppComponent]
})
export class AppModule { }
