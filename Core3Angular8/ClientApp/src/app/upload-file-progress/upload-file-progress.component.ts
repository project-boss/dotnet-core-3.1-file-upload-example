import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { UploadService } from '../services/upload.service';
@Component({
  selector: 'app-upload-file-progress',
  templateUrl: './upload-file-progress.component.html',
  styleUrls: ['./upload-file-progress.component.css']
})
export class UploadFileProgressComponent implements OnInit {

  form: FormGroup;
  progress: number = 0;

  constructor(
    public fb: FormBuilder,
    public fileUploadService: UploadService
  ) {
    this.form = this.fb.group({
      name: [''],
      files: [null]
    })
  }

  ngOnInit() { }

  uploadFile(event) {
    const file = (event.target as HTMLInputElement).files;
    this.form.patchValue({
      files: file
    });
    this.form.get('files').updateValueAndValidity()
  }

  submitUser() {
    // console.log(this.form.value.files);
    
    this.fileUploadService.addUser(
      this.form.value.name,
      this.form.value.files
    ).subscribe((event: HttpEvent<any>) => {
      switch (event.type) {
        case HttpEventType.Sent:
          console.log('Request has been made!');
          break;
        case HttpEventType.ResponseHeader:
          console.log('Response header has been received!');
          break;
        case HttpEventType.UploadProgress:
          this.progress = Math.round(event.loaded / event.total * 100);
          console.log(`Uploaded! ${this.progress}%`);
          break;
        case HttpEventType.Response:
          console.log('User successfully created!', event.body);
          setTimeout(() => {
            this.progress = 0;
          }, 1500);

      }
    })
  }

}
