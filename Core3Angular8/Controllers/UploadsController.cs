﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static Core3Angular8.Models.ImageUpload;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Core3Angular8.Controllers
{
    [Route("api/[controller]")]
    public class UploadsController : Controller
    {
        //Configuration FileUpload Example
        public static IWebHostEnvironment _environment;
        public UploadsController(IWebHostEnvironment environment)
        {
            _environment = environment;
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        //End Configuration

        //Post FileUpload Example
        //[HttpPost]
        //[RequestSizeLimit(100_000_000_000)]
        //public async Task<string> Post(FileUploadProduct objFile)
        //{
        //    try
        //    {

        //        if (objFile.files.Length > 0)
        //        {
        //            if (!Directory.Exists(_environment.WebRootPath + "//Uploads//"))
        //            {
        //                Directory.CreateDirectory(_environment.WebRootPath + "//Uploads//");
        //            }



        //            using (FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + "//Uploads//" + objFile.files.FileName))
        //            {
        //                objFile.files.CopyTo(fileStream);
        //                fileStream.Flush();

        //                var result = new
        //                {
        //                    files = "//Uploads//" + objFile.files.FileName,
        //                    id = objFile.Id,
        //                    name = objFile.Name
        //                };

        //                return result.ToString();
        //            }
        //        }
        //        else
        //        {
        //            return "Failed";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return ex.Message.ToString() + " : ddd";
        //    }

        //}





        [HttpPost("{sub}")]
        [RequestSizeLimit(100_000_000_000_000_000)]

        public async Task<IActionResult> OnPostUploadAsync(FileUploadProduct2 data)
        {

            //int maxSize = Int32.Parse(ConfigurationManager.AppSettings["MaxFileSize"]);
            //var size = data.files.Sum(f => f.Length);
            var random = RandomString(10);
            //ตรวจสอบว่ามี Folder Upload ใน wwwroot มั้ย
            if (!Directory.Exists(_environment.WebRootPath + "//Uploads//"))
            {
                Directory.CreateDirectory(_environment.WebRootPath + "//Uploads//"); //สร้าง Folder Upload ใน wwwroot
            }

            //var BaseUrl = url.ActionContext.HttpContext.Request.Scheme;
            // path ที่เก็บไฟล์
            var filePath = _environment.WebRootPath + "//Uploads//";


            foreach (var formFile in data.files.Select((value, index) => new { Value = value, Index = index }))
            //foreach (var formFile in data.files)
            {
                string filePath2 = formFile.Value.FileName;
                string filename = Path.GetFileName(filePath2);
                string ext = Path.GetExtension(filename);
                if (formFile.Value.Length > 0)
                {
                    // using (var stream = System.IO.File.Create(filePath + formFile.Value.FileName))
                    using (var stream = System.IO.File.Create(filePath + random + ext))
                    {
                        await formFile.Value.CopyToAsync(stream);
                    }
                }
            }

            return Ok(new { count = data.files.Count, filePath, data.Id, data.Name });

        }


    }
}
