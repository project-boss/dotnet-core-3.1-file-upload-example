﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Core3Angular8.Models;
using Core3Angular8.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using static Core3Angular8.ViewModels.MemberViewModel;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Core3Angular8.Controllers
{


    [Route("api/[controller]")]
    public class MemberController : Controller
    {

        private readonly AppDbContext _context;
        public MemberController(AppDbContext context, IWebHostEnvironment environment)
        {
            _context = context;

        }
        // GET: api/values
        [HttpGet]
        public object Member()
        {
            var data = _context.Members
               .Include(m => m.WorkgroupId);
            //.Where(x => x.Id == id).First();
            return data;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // GET api/values/5
        [HttpPost]
        public object Post(MemberWorkViewM data)
        {
            //https://www.c-sharpcorner.com/article/the-new-json-serializer-in-net-core-3/ 
            var people = JsonConvert.DeserializeObject<List<Members>>(data.subjects);
            foreach (var item in people)
            {
                var myType = new Member
                {
                    Name = item.name,
                    Email = item.email,
                    WorkgroupId = 1,
                    JoinDate = new DateTime()
                };
                _context.Members.Add(myType);
            }
            _context.SaveChanges();

            return people;
        }


        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
