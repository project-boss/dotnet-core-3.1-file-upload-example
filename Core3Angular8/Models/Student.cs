﻿using System;
namespace Core3Angular8.Models
{
    public class Student
    {
        public int Id { get; set; } = 0;
        public string Name { get; set; } = "";
        public int Role { get; set; } = 0;
    }
}
