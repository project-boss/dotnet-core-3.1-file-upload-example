﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core3Angular8.Models
{
    [Table("Members")]
    [Description("ตารางสมาชิกในกลุ่มงาน")]
    public class Member
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Description("PK")]
        public long Id { get; set; }

        [ForeignKey("Workgroup")]
        [Description("FK: กลุ่มงาน")]
        public long WorkgroupId { get; set; }
        public virtual Workgroup Workgroup { get; set; }

        //[ForeignKey("DipUser")]
        //[Description("FK: ผู้ใช้งาน")]
        //public long DipUserId { get; set; }
        //public virtual DipUser DipUser { get; set; }

        [Required]
        [Description("ชื่อหัวหน้ากลุ่ม")]
        public string Name { get; set; }

        [Required]
        [Description("ชื่อหัวหน้ากลุ่ม")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Description("วันที่เข้ากลุ่ม")]
        public DateTime JoinDate { get; set; }

        [DataType(DataType.Date)]
        [Description("วันที่ออกจากกลุ่ม")]
        public DateTime? LeaveDate { get; set; }

        /* งานที่รับผิดชอบ */
        public ICollection<MemberWork> MemberWorks { get; set; }
    }
}
