﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core3Angular8.Models
{
    /// <summary>
    /// กลุ่มงาน
    /// </summary>
    [Table("Workgroups")]
    [Description("ตารางกลุ่มงาน")]
    public class Workgroup
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Description("PK")]
        public long Id { get; set; }

        [Required]
        [Description("ชื่อหัวหน้ากลุ่ม")]
        public string Name { get; set; }

        /* ผู้ปฏิบัติงานในกลุ่มงาน */
        public ICollection<Member> Members { get; set; }

        /* คลาสผลิตภัณฑ์หลักที่รับผิดชอบ */
        //public ICollection<WorkgroupProductClass> WorkgroupProductClasses { get; set; }
    }
}
