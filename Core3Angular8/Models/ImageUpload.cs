﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace Core3Angular8.Models
{
    public class ImageUpload
    {
        public class FileUploadProduct
        {
            public IFormFile files { get; set; }
            public long Id  { get; set; }
            public string Name { get; set; }
        }

        public class FileUploadProduct2
        {
            public List<IFormFile> files { get; set; }
            public long Id { get; set; }
            public string Name { get; set; }
        }
    }
}
