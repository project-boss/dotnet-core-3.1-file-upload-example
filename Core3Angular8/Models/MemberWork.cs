﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core3Angular8.Models
{
    [Table("MemberWorks")]
    [Description("ตารางงานที่ได้รับมอบหมาย")]
    public class MemberWork
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Description("PK")]
        public long Id { get; set; }


        //[ForeignKey("Minion")]
        //[Description("FK: กลุ่มผู้ปฏิบัติงาน")]
        //public long MemberId { get; set; }
        //public virtual Member Member { get; set; }

        //[ForeignKey("MemberType")]
        //[Description("FK: กลุ่มประเภทผู้ปฏิบัติงาน")]
        //public long MemberTypeId { get; set; }
        //public virtual MemberType MemberType { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Description("วันที่เริ่มต้น")]
        public DateTime StartDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Description("วันที่สิ้นสุด")]
        public DateTime EndDate { get; set; }

        [Required]
        [Description("จำนวนงานที่มอบหมาย")]
        public int Target { get; set; }
    }
}
