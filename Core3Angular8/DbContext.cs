﻿using System;
using Core3Angular8.Models;
using Microsoft.EntityFrameworkCore;
using static Core3Angular8.Models.ContractClass;

namespace Core3Angular8
{
    public class AppDbContext : DbContext
    {
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<MemberWork> MemberWorks { get; set; }
        public DbSet<Workgroup> Workgroups { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        { }
    }
}
